﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public bool gameActive = false;
    public float timeInactive = 0f;
    public bool gameWasReset = true;
    public float timer = 1f;
    private bool endReached = false;

    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public Canvas canvasPrefab;
    public GameObject playerPrefab;
    public Goal goalPrefab;

    public Canvas canvasInstance;
    public GameObject playerInstance;
    public Goal goalInstance;
    private Camera cameraInstance;
    private ResetManager resetManager;

    private Text timerText;
    private Text levelText;
    private Text messageText;
    private GameObject theEndText;
    public string[] messages = new string[20];

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            _instance = this;
        }
        
    }

    void Start()
    {        
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        gameWasReset = true;
        gameActive = false;
        var playerPos = GameObject.Find("PlayerPos").transform.position;
        playerInstance = GameObject.Instantiate(playerPrefab, playerPos, Quaternion.identity);
        cameraInstance = playerInstance.GetComponentInChildren<Camera>();
        cameraInstance.clearFlags = CameraClearFlags.SolidColor;

        var goalPos = GameObject.Find("GoalPos").transform.position;
        goalInstance = GameObject.Instantiate(goalPrefab, goalPos, Quaternion.identity);
        resetManager = GameObject.FindObjectOfType<ResetManager>();
        resetManager.RegisterObjects();
        canvasInstance = GameObject.Instantiate(canvasPrefab);
        timerText = GameObject.FindGameObjectWithTag("Timer").GetComponent<Text>();
        levelText = GameObject.FindGameObjectWithTag("Level").GetComponent<Text>();
        levelText.text = (SceneManager.GetActiveScene().buildIndex).ToString();
        messageText = GameObject.FindGameObjectWithTag("Message").GetComponent<Text>();
        messageText.text = string.Empty;
        theEndText = GameObject.FindGameObjectWithTag("TheEnd");

        if (SceneManager.GetActiveScene().buildIndex == 14)
        {
            goalInstance.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public void StartCountdown()
    {
        if (!gameActive && gameWasReset)
        {
            playerInstance.GetComponent<Rigidbody2D>().isKinematic = false;
            gameActive = true;
            gameWasReset = false;
            timeInactive = 0f;
        }
    }
    
    private Color freezeBG = new Color(0.5f, 0.5f, 0.5f, 1f);
    private Color goBG = new Color(0.2f, 0.5f, 0.7f, 1f);

    private void Update()
    {
        if (gameActive)
        {
            GameManager.Instance.timer -= Time.deltaTime;
            timerText.text = GameManager.Instance.timer.ToString("n2");
            if (GameManager.Instance.timer <= 0)
            {
                GameManager.Instance.timer = 0.0f;
                timerText.text = GameManager.Instance.timer.ToString("n2");
                gameActive = false;
                messageText.text = messages[SceneManager.GetActiveScene().buildIndex];
            }

            
            cameraInstance.backgroundColor = Color.Lerp(freezeBG, goBG, 1f);
        }
        else
        {            
            cameraInstance.backgroundColor = Color.Lerp(goBG, freezeBG, 1f);
        }

        if (Input.GetKeyDown(KeyCode.R) && !endReached)
        {
            gameActive = false;
            gameWasReset = true;
            GameManager.Instance.timer = 1.0f;
            ResetScene();

            timerText.text = GameManager.Instance.timer.ToString("n2");            
        }

        if (!endReached)
        {
            if (timeInactive > 6.0f)
            {
                messageText.text = "Press 'R' to reset";
            }
            else
            {
                timeInactive += Time.deltaTime;
                messageText.text = messages[SceneManager.GetActiveScene().buildIndex];
            }
        }
    }

    public void AdvanceToNextLevel()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (SceneManager.GetActiveScene().buildIndex == 14)
        {
            endReached = true;
            theEndText.gameObject.GetComponent<Text>().enabled = true;
            messageText.text = string.Empty;
            return;
        }
        if (SceneManager.sceneCountInBuildSettings > nextSceneIndex)
        {
            SceneManager.LoadScene(nextSceneIndex);

            timer = 1.0f;
        }
        
    }

    public void ResetScene()
    {
        resetManager.ResetScene();
    }
}
