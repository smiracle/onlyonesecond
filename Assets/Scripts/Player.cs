﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
    [Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;          // Amount of maxSpeed applied to crouching movement. 1 = 100%
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
    [SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
    [SerializeField] private Transform m_CeilingCheck;                          // A position marking where to check for ceilings
    [SerializeField] private Collider2D m_CrouchDisableCollider;                // A collider that will be disabled when crouching
    [SerializeField] private bool isGrounded;
    [SerializeField] private bool isJumping = false;

    const float k_GroundedRadius = .4f; // Radius of the overlap circle to determine if grounded    
    const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
    private Rigidbody2D rigidbody;
    private BoxCollider2D[] colliders;
    private bool isFacingRight = true;  // For determining which way the player is currently facing.
    private Vector3 velocity = Vector3.zero;
    private Animator animator;
    public float runSpeed = 40f;
    float horizontalMove = 0f;
    float maxJumpSpeed = 10f;

    bool crouch = false;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        colliders = GetComponents<BoxCollider2D>();
    }

    private void Start()
    {
        Reset();
    }

    public void Update()
    {
        
        
        //if (horizontalMove != 0 || Input.GetButtonDown("Jump") || Input.GetButtonDown("Crouch"))
        //{
        //    GameManager.Instance.StartCountdown();
        //}
        if (Input.anyKeyDown && !Input.GetKey(KeyCode.R))
        {
            GameManager.Instance.StartCountdown();
        }
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        Animate();
                
    }

    private void FixedUpdate()
    {
        isGrounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                isGrounded = true;
        }

        // Move our character
        if (GameManager.Instance.gameActive)
        {
            Move(horizontalMove * Time.fixedDeltaTime, crouch, isJumping);
        }

        isJumping = false;
    }

    private void Animate()
    {
        if (Input.GetButtonDown("Jump"))
        {
            isJumping = true;
            animator.SetBool("IsJumping", true);
        }
        animator.SetBool("IsJumping", rigidbody.velocity.y > 0 && !isGrounded);

        //if (animator.GetBool("IsJumping"))
        //{
        //    Debug.Log(rigidbody.velocity.y > 0);
        //    animator.SetBool("IsJumping", rigidbody.velocity.y > 0);
        //}

        if (Input.GetButtonDown("Crouch") && GameManager.Instance.gameActive)
        {
            crouch = true;
            animator.SetBool("IsCrouching", true);
        }
        else if (Input.GetButtonUp("Crouch") && GameManager.Instance.gameActive)
        {
            crouch = false;
            animator.SetBool("IsCrouching", false);
        }
        if(rigidbody.velocity.x > 0.001f || rigidbody.velocity.x < -0.001f)
        {
            animator.SetBool("IsRunning", true);
        }
        else         
        {
            animator.SetBool("IsRunning", false);
        }

        
        if (rigidbody.velocity.x > 0.001f && !isFacingRight)
        {        
            Flip();
        }        
        else if (rigidbody.velocity.x < -0.001f && isFacingRight)
        {        
            Flip();
        }
    }


    public void Reset()
    {
        StopAllAnimations();
        horizontalMove = 0f;
        ResetPhysics();
    }

    private void StopAllAnimations()
    {
        animator.SetBool("IsJumping", false);
        animator.SetBool("IsCrouching", false);
        animator.SetBool("IsRunning", false);
    }



    public void ResetPhysics()
    {
        velocity = Vector3.zero;
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = 0f;
        rigidbody.isKinematic = true;        
    }

    public void Move(float move, bool crouch, bool jump)
    {
        // If crouching, check to see if the character can stand up
        if (!crouch)
        {
            // If the character has a ceiling preventing them from standing up, keep them crouching
            if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
            {
                crouch = true;
            }
        }

        //only control the player if grounded or airControl is turned on
        if (isGrounded || m_AirControl)
        {

            // If crouching
            if (crouch)
            {
                // Reduce the speed by the crouchSpeed multiplier
                move *= m_CrouchSpeed;

                // Disable one of the colliders when crouching
                if (m_CrouchDisableCollider != null)
                    m_CrouchDisableCollider.enabled = false;
            }
            else
            {
                // Enable the collider when not crouching
                if (m_CrouchDisableCollider != null)
                    m_CrouchDisableCollider.enabled = true;
            }

            // Move the character by finding the target velocity
            Vector3 targetVelocity = new Vector2(move * 10f, rigidbody.velocity.y);
            // And then smoothing it out and applying it to the character
            rigidbody.velocity = Vector3.SmoothDamp(rigidbody.velocity, targetVelocity, ref velocity, m_MovementSmoothing);

            
        }
        // If the player should jump...
        if (isGrounded && jump)
        {
            // Add a vertical force to the player.
            isGrounded = false;
            if (rigidbody.velocity.y < maxJumpSpeed)
            {
                rigidbody.AddForce(new Vector2(0f, m_JumpForce));
            }
        }
    }


    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        isFacingRight = !isFacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}