﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenBlocker : MonoBehaviour
{
    float pauseTime = 1f;
    float time = 0f;
    bool IsReset = true;
    bool IsCrushed = false;

    //[SerializeField] private LayerMask m_WhatIsGround;
    //[SerializeField] private Transform m_GroundCheck; 
    //[SerializeField] private Transform m_CeilingCheck;
    //public bool m_Grounded;
    //const float k_GroundedRadius = .4f;

    public float crushSpeed = 15f;
    public float resetSpeed = 3f;
    private Vector3 resetPosition;
    public Transform target;

    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        resetPosition = gameObject.transform.position;
    }
    void Update()
    {
        if (IsReset)
        {
            time += Time.deltaTime;
        }
    }
    public void Reset()
    {
        IsReset = true;
        IsCrushed = false;
        time = 0f;
    }

    private void FixedUpdate()
    {
        
        if (IsReset && time >= pauseTime)
        {
            //Crush
            transform.position = Vector3.MoveTowards(transform.position, target.position, crushSpeed * Time.deltaTime);            
            if (Vector3.Distance(transform.position, target.position) < 0.001f && !IsCrushed)
            {
                IsCrushed = true;
                IsReset = false;
                time = 0f;
            }
        }
        else if (IsCrushed)
        {
            //Reset
            transform.position = Vector3.MoveTowards(transform.position, resetPosition, resetSpeed * Time.deltaTime);
            if (Vector3.Distance(transform.position, resetPosition) < 0.001f && !IsReset)
            {
                IsReset = true;
                IsCrushed = false;
            }
        }
    }
}
