﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetManager : MonoBehaviour
{
    public GameObject[] objectsInScene = new GameObject[0];
    private Vector3[] startPositions = new Vector3[0];
    private GameObject player;
    private Vector3 playerStartPosition;


    public void RegisterObjects()
    {
        player = GameManager.Instance.playerInstance;
        playerStartPosition = player.transform.position;
        startPositions = new Vector3[objectsInScene.Length];
        for(int i =0; i < objectsInScene.Length; i++)
        {
            startPositions[i] = objectsInScene[i].transform.position;
        }
        
    }

    public void ResetScene()
    {
        for (int i = 0; i < objectsInScene.Length; i++)
        {
            objectsInScene[i].transform.position = startPositions[i];
            if(objectsInScene[i].GetComponent<RedBlocker>()!=null)
            {
                objectsInScene[i].GetComponent<RedBlocker>().Reset();
            }
        }
        player.transform.position = playerStartPosition;
        player.GetComponent<Player>().Reset();
    }
}
