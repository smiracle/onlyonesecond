﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public Transform target;  //what the camera is following
    private float speed = 15f;
    Vector3 offset;
    

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        // The difference between where the camera starts and where the player is
        offset = transform.position - target.position;
    }

    void FixedUpdate()
    {        
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, speed * Time.deltaTime);
    }    
}
