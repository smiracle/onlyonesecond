﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public bool PlayMusic = true;
    public AudioClip Music_Breeze;
    private AudioSource Source;

    // Start is called before the first frame update
    void Start()
    {
        Source = GetComponent<AudioSource>();
        if (Music_Breeze != null)
        {
            Source.clip = Music_Breeze;
            Source.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
